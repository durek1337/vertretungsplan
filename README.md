# Vertretungsplan
## Konfiguration
* Konfigurieren über "js/config.js"
* "index_pupil.html" und "index_teacher.html" können namentlich verändert werden. Sie unterscheiden sich nur durch die Variable "mode"
* Aufruf nur über http(s)://, nicht über file://

## Funktionsweise
Nummeriere Dateinamen ab "config.initialNum" durch und prüfe jeden Schritt, ob die Seite existiert. Wenn nicht, gehe solange weiter bis "config.maxGap"-mal keine Seite verfügbar war. Cache alle gefundenen Seiten und zeige alle vorhandenen mit Zeitabstand "config.showTime" an. Sobald alle durchlaufen wurden, wiederhole den Prozess.
