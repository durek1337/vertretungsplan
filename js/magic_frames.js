function pad(num, size) {
    num = num.toString();
    while (num.length < size) num = "0" + num;
    return num;
}

var magic = {
  today : {
    $selector : $("#leftframe"),
    $status : $("#leftstatus"),
    current : config.initialNum-1,
    next : [],
    cache : [],
    totalPages : null,
    currentPage : null
  },
  tomorrow: {
    $selector:  $("#rightframe"),
    $status : $("#rightstatus"),
    current: config.initialNum-1,
    next : [],
    cache : [],
    totalPages : null,
    currentPage : null
  }
}

function showNext(d){
  var obj = magic[d]
  console.log("Show next",d,"of queue",obj.current+1,obj.next)
  /*
    if(obj.totalPages == 0){
      obj.$selector.text('Keine Seiten vorhanden')
      return false
    }
  */
  if(!obj.next.length){
    console.log("Pages went through, repeat");
    obj.totalPages = 0;
    obj.current = config.initialNum-1;
    return checkNext(d);
  }
  obj.current++
  var t = obj.next.shift()
  var data = obj.cache.shift()
  if(t === false)
    showNext(d)
  else {
    obj.currentPage++;
    obj.$status.text("Seite "+obj.currentPage+" / "+obj.totalPages+" - "+date.format(new Date(t),"HH:mm:ss"))

//    obj.$selector.attr("src",config.paths[mode][d]+"/"+config.fileName(obj.current)+"?t="+t)
    var doc = obj.$selector[0].contentWindow.document
      doc.open()
      doc.write(data)
      doc.close()
    setTimeout(function(){
      showNext(d)
    },config.showTime)
  }
}
function getNext(d){ // d = "today" || "tomorrow"
    var obj = magic[d]
    var current = magic[d].current
    for(var i=0;i<obj.next.length;i++){
      if(magic[d].next[i] !== false) return current+i+1
    }
    return null
}
function checkNext(d){ // d = "today" || "tomorrow"
  var obj = magic[d]
  var check = obj.current+obj.next.length+1
  var time = Date.now()

    $.ajax(config.paths[mode][d]+"/"+config.fileName(pad(check,config.digits))+"?t="+time, {
      cache: true,
      contentType: "application/html; charset=utf-8",
      dataType: "html",
      mimeType : config.fileMimeType
      /*
      beforeSend: function( xhr ) {
        xhr.overrideMimeType( "text/plain; charset=utf-8" );
      }
      */
    })
    .done(function(data, textStatus, jqXHR){
      console.log("Request succeed",textStatus)
      obj.next.push(time)
      obj.cache.push(data)
    })
    .fail(function(jqXHR, textStatus, errorThrown){
      console.log("Request failed",textStatus,errorThrown)
      obj.next.push(false)
      obj.cache.push(null)
    })
    .always(function(){
      var countFails = 0;
      console.log("Preparing next step",obj.current,obj.next)
      if(obj.next.length >= config.maxGap)
      for(var i=obj.next.length-1;i>0;i--){
        if(obj.next[i] === false) countFails++
      }
      if(countFails <= config.maxGap)
        checkNext(d)
      else {
        obj.totalPages = obj.next.filter(function(el){
          return el !== false
        }).length // Setze totalPages auf die Anzahl aller gültigen Seiten
        if(obj.totalPages == 0){ // Wenn insgesamt 0 gültige Seiten gefunden wurden
          if(obj.$status.text() !== config.noPagesFoundStatus) // Wenn Status nicht schon zuvor geändert
          obj.$selector.attr("src",obj.$selector.attr("src")) // Setze Inhalt der Seite wieder auf seinen Initialwert, indem das src-Attribut mit seinem eigenen Wert gesetzt wird.
          obj.$status.text(config.noPagesFoundStatus) // Setze Status, dass keine Seiten vorhanden sind
        }
        obj.currentPage = 0;
        setTimeout(function(){
          showNext(d) // Fang an die Seiten anzuzeigen
        },config.showTimeLoadingPage)        
      }
    })

}


$(function(){ // Führe erst aus, wenn das DOM geladen wurde
  console.log("DOM loaded.")
  setTimeout(function(){checkNext("today")},config.showTimeLoadingPage)
  setTimeout(function(){checkNext("tomorrow")},config.showTimeLoadingPage)


})
