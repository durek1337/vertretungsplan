var config = {
  maxGap : 3, // Der maximal-zulässige Abstand zwischen durchnummerierten Plänen bis ein Ende angenommen wird
  digits : 3, // Anzahl der Ziffern mit führenden Nullen, welche durchnummeriert werden. 3 => 001, 092, 123
  showTime : 5000, // Anzeigedauer pro Seite in Millisekunden
  fileMimeType : "text/plain; charset=iso-8859-1", // Der verwendete Zeichensatz der Quelldateien. Hier im Beispiel müsste es statt iso-8859-1, utf-8 sein, um die Umlaute korrekt darzustellen
  initialNum : 1, // Erste zu prüfende durchnummerierte Seite
  showTimeLoadingPage : 500, // Ladeseite anzeigen lassen in Millisekunden bis die Seiten angefragt werden
  noPagesFoundStatus : "Keine neuen Seiten gefunden",
  paths : {
    pupil : {
      today : "./example", // schueler_heute
      tomorrow : "./example", // schueler_morgen
    },
    teacher : {
      today : "./example", // teacher_heute
      tomorrow : "./example" // teacher_morgen
    }
  },
  fileName : function(i){ // Schema der Dateinamen mit variabler Durchnummerierung
    return "subst_"+i+".html"
  }
}
